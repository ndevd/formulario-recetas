<?php

if( isset($_POST['nombre']) && isset($_POST['ingredientes']) && isset($_POST['preparacion']) ){

    $nom = $_POST['nombre'];
    $ing = $_POST['ingredientes'];
    $pre = $_POST['preparacion'];


    echo <<<_END
    <!DOCTYPE html>
    <html lang="es">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Ver receta</title>
        <link rel="shortcut icon" type="image/x-icon" href="./assets/img/cooking_favicon.ico">
        <link rel="stylesheet" href="./assets/css/style2.css">
    </head>
    <body>
        <h2><i>La receta de la abuela</i></h2>
        
        <div>
            <table border="1">
                <tbody>
                    <tr>
                        <th>Nombre: </th>
                    </tr>
                    <tr>
                        <td>$nom</td>
                    </tr>
                    <tr>
                        <th>Ingredientes: </th>
                    </tr>
                    <tr>
                        <td>$ing</td>
                    </tr>
                    <tr>
                        <th>Preparacion: </th>    
                    </tr>
                    <tr>
                        <td>$pre</td>
                    </tr>    
                </tbody>
            </table>
        </div>
        <br><br>

        <a href="textarea_receta.html">Volver al formulario</a>
    </body>
    </html>
    _END;

}else{
    echo <<<_END
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <title>Error!</title>
            <link rel="shortcut icon" type="image/x-icon" href="./assets/img/cooking_favicon.ico">
            <link rel="stylesheet" href="./assets/css/style2.css">
        </head>
        <body>
            <h2><i>La receta de la abuela</i></h2>

            <p id="error">Error, debes llenar el <a href="textarea_receta.html">formulario</a> completo!.</p>
        </body>
        </html>
        _END;
}
?>